using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour
{
    private Animator animator;
    public Vector3 jump;
    public float jumpForce = 2.0f;
    private Rigidbody2D rb2d;
    private SpriteRenderer mySpriteRenderer;
    private bool teclapulsada;
    
    
    public bool isGrounded;
    private bool Cayendo;

    public float velocidad;
    // Start is called before the first frame update
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        mySpriteRenderer = GetComponent<SpriteRenderer>();
    }

    
    

    // Update is called once per frame
    void Update()
    {
        animator.SetBool("isGrounded", isGrounded);
        animator.SetBool("teclapulsada", teclapulsada);
        animator.SetFloat("Speedy", Mathf.Abs(rb2d.velocity.y));
        animator.SetBool("Cayendo", Cayendo);
        if (Input.GetKey(KeyCode.A))
        {
            transform.Translate(velocidad * Vector3.left);
            mySpriteRenderer.flipX = true;
        }
        if (Input.GetKey(KeyCode.D))
        {
            transform.Translate(velocidad * Vector3.right);
            animator.SetBool("Correr", true);
            mySpriteRenderer.flipX = false;
        }
        if (Input.anyKey)
        {
            teclapulsada = true;
            animator.SetBool("teclapulsada", teclapulsada);
        }
        else
        {
            animator.SetBool("Correr", false);
            teclapulsada = false;
        }

        
        RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.down, 1.8f, LayerMask.GetMask("floor"));
        if ((hit.collider != null) && (hit.collider.CompareTag("floor")))
        {
            isGrounded = true;
        }
        else
        {
            isGrounded = false;
        }
        if (rb2d.velocity.y < 0)
        {
            Cayendo = true;
        }
        if (rb2d.velocity.y > 0)
        {
            Cayendo = false;
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (isGrounded==true)
            {
                salto();
            }
        }
    }

    private void salto()
    {
        rb2d.velocity = new Vector2(rb2d.velocity.x, jumpForce);
        isGrounded = false;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Enemigo"))
        {
            Destroy(gameObject);
            GameManager.muerto = true;
        }

    }
}
